use crate::models::PluginID;
use chrono::{DateTime, Duration, Utc};
use serde::{Deserialize, Serialize};
use std::fs;
use std::io::{Error, ErrorKind};
use std::path::{Path, PathBuf};

static SYNC_AMOUNT_DEFAULT: u32 = 300;
static CONCURRENT_DOWNLOADS_DEFAULT: u32 = 24;
static CONFIG_NAME: &str = "newsflash.json";

#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    backend: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    concurrent_downlaods: Option<u32>,
    sync_amount: u32,
    #[serde(with = "json_time")]
    last_sync: DateTime<Utc>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    keep_articles_days: Option<u64>,
    #[serde(skip_serializing)]
    #[serde(skip_deserializing)]
    path: PathBuf,
}

impl Config {
    pub fn open(path: &Path) -> Result<Self, Error> {
        let path = path.join(CONFIG_NAME);
        if path.as_path().exists() {
            let data = fs::read_to_string(&path)?;
            let mut config: Self = serde_json::from_str(&data).map_err(|_| Error::new(ErrorKind::InvalidData, "Failed to parse Config"))?;
            config.path = path;
            return Ok(config);
        }

        let config = Config {
            backend: None,
            sync_amount: SYNC_AMOUNT_DEFAULT,
            concurrent_downlaods: Some(CONCURRENT_DOWNLOADS_DEFAULT),
            last_sync: Utc::now() - Duration::try_weeks(1).unwrap(),
            keep_articles_days: None,
            path,
        };
        config.write()?;
        Ok(config)
    }

    fn write(&self) -> Result<(), Error> {
        let data = serde_json::to_string_pretty(self).map_err(|_| Error::new(ErrorKind::InvalidData, "Failed to serialize Config"))?;
        fs::write(&self.path, data)?;
        Ok(())
    }

    pub fn get_backend(&self) -> Option<PluginID> {
        self.backend.as_ref().map(|plugin_id| PluginID::new(plugin_id))
    }

    pub fn set_backend(&mut self, backend: Option<&PluginID>) -> Result<(), Error> {
        self.backend = backend.map(|plugin_id| plugin_id.as_str().to_owned());
        self.write()?;
        Ok(())
    }

    pub fn get_sync_amount(&self) -> u32 {
        self.sync_amount
    }

    pub fn set_sync_amount(&mut self, amount: u32) -> Result<(), Error> {
        self.sync_amount = amount;
        self.write()?;
        Ok(())
    }

    pub fn get_last_sync(&self) -> DateTime<Utc> {
        self.last_sync
    }

    pub fn set_last_sync(&mut self, time: DateTime<Utc>) -> Result<(), Error> {
        self.last_sync = time;
        self.write()?;
        Ok(())
    }

    pub fn get_keep_articles_duration(&self) -> Option<Duration> {
        self.keep_articles_days.map(|days| Duration::try_days(days as i64).unwrap())
    }

    pub fn set_keep_articles_duration(&mut self, duration: Option<Duration>) -> Result<(), Error> {
        if let Some(duration) = duration {
            if duration < Duration::try_days(1).unwrap() {
                log::error!("Duration to keep articles is not allowed to be < 1 Day. Duration: {}", duration);
                return Err(Error::new(ErrorKind::Other, "Duration too low"));
            }
        }
        self.keep_articles_days = duration.map(|d| d.num_days() as u64);
        self.write()?;
        Ok(())
    }

    pub fn get_concurrent_downloads(&self) -> u32 {
        self.concurrent_downlaods.unwrap_or(CONCURRENT_DOWNLOADS_DEFAULT)
    }
}

mod json_time {
    use chrono::{DateTime, Utc};
    use serde::{de::Error, Deserialize, Deserializer, Serialize, Serializer};

    pub fn time_to_json(t: DateTime<Utc>) -> String {
        t.to_rfc3339()
    }

    pub fn serialize<S: Serializer>(time: &DateTime<Utc>, serializer: S) -> Result<S::Ok, S::Error> {
        time_to_json(*time).serialize(serializer)
    }

    pub fn deserialize<'de, D: Deserializer<'de>>(deserializer: D) -> Result<DateTime<Utc>, D::Error> {
        let time: String = Deserialize::deserialize(deserializer)?;
        Ok(DateTime::parse_from_rfc3339(&time).map_err(D::Error::custom)?.with_timezone(&Utc))
    }
}
