mod error;

pub use self::error::FeedParserError;
use crate::models::{Feed, FeedID, Url};
use bytes::Bytes;
use feed_rs::parser;
use libxml::{parser::Parser, xpath::Context};
use log::{debug, error, warn};
use mime::Mime;
use reqwest::Client;
use std::{cmp::Ordering, sync::Arc};
use tokio::sync::Semaphore;

#[derive(Clone, Debug)]
pub enum ParsedUrl {
    SingleFeed(Box<Feed>),
    MultipleFeeds(Vec<Feed>),
}

struct Content {
    pub encoding: String,
    pub content_type: Option<ContentType>,
}

#[derive(Debug, PartialEq)]
enum ContentType {
    Rss,
    Atom,
    JsonFeed,
    Html,
}

pub async fn download_and_parse_feed(
    url: &Url,
    id: &FeedID,
    title: Option<String>,
    semaphore: Arc<Semaphore>,
    client: &Client,
) -> Result<ParsedUrl, FeedParserError> {
    let permit = semaphore.acquire().await?;

    let result = client
        .get(url.as_str())
        .send()
        .await
        .map_err(|err| {
            log::error!("get request failed: {}", err);
            FeedParserError::Http(err)
        })?
        .error_for_status()
        .map_err(|err| {
            error!("Downloading feed failed: {}", url);
            FeedParserError::Http(err)
        })?;

    let redirected_url = Url::new(result.url().clone());
    let base_url = redirected_url.base().unwrap_or(redirected_url.clone());

    let sniffed_content = check_content_type(result.headers().get(reqwest::header::CONTENT_TYPE));

    let result_bytes = result.bytes().await.map_err(|err| {
        error!("Reading response as bytes failed: {redirected_url}");
        err
    })?;

    if let Some(sniffed_content) = sniffed_content {
        if sniffed_content.content_type == Some(self::ContentType::Html) {
            debug!("ContentType: Html -> trying to parse page for feed url");
            if let Ok(feed_vec) = parse_html(&result_bytes, &sniffed_content.encoding, &base_url) {
                match feed_vec.len().cmp(&1) {
                    Ordering::Greater => return Ok(ParsedUrl::MultipleFeeds(feed_vec)),
                    Ordering::Less => return Err(FeedParserError::Html),
                    Ordering::Equal => {
                        if let Some(Feed {
                            feed_id: _,
                            label: _,
                            website: _,
                            feed_url: Some(url),
                            icon_url: _,
                            error_count: _,
                            error_message: _,
                        }) = feed_vec.first()
                        {
                            let new_result = client.get(url.as_str()).send().await?;
                            let new_result_bytes = new_result.bytes().await?;
                            return Ok(ParsedUrl::SingleFeed(parse_feed(new_result_bytes, url, id, title)?));
                        }
                    }
                }
            }
        }
    }

    drop(permit);

    Ok(ParsedUrl::SingleFeed(parse_feed(result_bytes, &redirected_url, id, title)?))
}

fn parse_html(html: &Bytes, encoding: &str, base_url: &Url) -> Result<Vec<Feed>, FeedParserError> {
    let parser = Parser::default_html();
    let parser_options = libxml::parser::ParserOptions {
        encoding: Some(encoding),
        ..Default::default()
    };
    let xpath = "//link[@rel='alternate']";
    if let Ok(doc) = parser.parse_string_with_options(html, parser_options) {
        if let Ok(xpath_ctx) = Context::new(&doc) {
            if let Ok(xpath_result) = xpath_ctx.evaluate(xpath) {
                let xpath_result = xpath_result.get_nodes_as_vec();
                let mut result_vec: Vec<Feed> = Vec::new();

                if xpath_result.is_empty() {
                    warn!("xpath didn't yield any results: {}", xpath);
                    return Err(FeedParserError::Html);
                }

                for node in xpath_result {
                    if let Some(url) = node.get_property("href") {
                        if url.starts_with("android-app") || url.starts_with("ios-app") {
                            continue;
                        }
                        debug!("Parsing Html yielded feed: {}", url);
                        let title = match node.get_property("title") {
                            Some(title) => title,
                            None => url.clone(),
                        };

                        let url = match Url::parse(&url) {
                            Ok(url) => url,
                            Err(_) => match base_url.clone().join(&url) {
                                Ok(url) => Url::new(url),
                                Err(err) => {
                                    warn!("Failed to parse url: {} - {}", url, err);
                                    continue;
                                }
                            },
                        };

                        match node.get_property("type") {
                            None => continue,
                            Some(_type) => {
                                if !_type.contains("rss") && !_type.contains("atom") && !_type.contains("xml") {
                                    continue;
                                }
                            }
                        }

                        result_vec.push(Feed {
                            feed_id: FeedID::new(url.as_str()),
                            label: title,
                            website: None,
                            feed_url: Some(url),
                            icon_url: None,
                            error_count: 0,
                            error_message: None,
                        });
                    } else {
                        warn!("<link> tag is missin href property");
                    }
                }

                return Ok(result_vec);
            } else {
                warn!("xpath evaluation failed: {}", xpath);
            }
        }
    } else {
        warn!("Failed to parse HTML");
    }

    Err(FeedParserError::Html)
}

fn check_content_type(header: Option<&reqwest::header::HeaderValue>) -> Option<Content> {
    if let Some(header) = header {
        if let Ok(header) = header.to_str() {
            if let Ok(mime) = header.parse::<Mime>() {
                let charset = mime.get_param("charset").map(|charset| charset.as_str()).unwrap_or("utf-8");

                let essence = mime.essence_str();
                let content_type = if essence == "text/html" {
                    Some(ContentType::Html)
                } else if essence == "application/atom" {
                    Some(ContentType::Atom)
                } else if essence == "text/xml" || essence == "application/rss" {
                    Some(ContentType::Rss)
                } else if essence == "application/vnd.api" && mime.suffix().map(|name| name.as_str()) == Some("json") {
                    Some(ContentType::JsonFeed)
                } else {
                    None
                };

                return Some(Content {
                    encoding: charset.into(),
                    content_type,
                });
            }
        }
    }

    None
}

fn parse_feed(feed: Bytes, url: &Url, id: &FeedID, title: Option<String>) -> Result<Box<Feed>, FeedParserError> {
    let parser = parser::Builder::new().base_uri(url.base().ok().as_deref()).build();

    if let Ok(feed) = parser.parse(feed.as_ref()) {
        let mut feed = Feed::from_feed_rs(feed, title, url);
        feed.feed_id = id.clone();
        Ok(Box::new(feed))
    } else {
        Err(FeedParserError::Feed)
    }
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use super::ParsedUrl;
    use crate::models::{FeedID, Url};
    use crate::util::feed_parser;
    use reqwest::Client;
    use tokio::sync::Semaphore;

    #[tokio::test]
    pub async fn golem_atom() {
        let client = reqwest::ClientBuilder::new()
            .user_agent("Mozilla/5.0 (X11; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0")
            .use_native_tls()
            .hickory_dns(true)
            .gzip(true)
            .build()
            .unwrap();

        let url_text = "https://rss.golem.de/rss.php?feed=ATOM1.0";
        let url = Url::parse(url_text).unwrap();
        let feed_id = FeedID::new(url_text);
        let feed = feed_parser::download_and_parse_feed(&url, &feed_id, None, Arc::new(Semaphore::new(20)), &client)
            .await
            .unwrap();

        let feed = match feed {
            ParsedUrl::SingleFeed(feed) => feed,
            ParsedUrl::MultipleFeeds(_) => panic!("Expected Single Feed"),
        };

        assert_eq!(feed.label, "Golem.de");
        assert_eq!(feed.icon_url.unwrap().to_string(), "https://www.golem.de/favicon.ico");
        assert_eq!(feed.website.unwrap().to_string(), "https://www.golem.de/");
    }

    #[tokio::test]
    pub async fn golem_rss() {
        let url_text = "https://rss.golem.de/rss.php?feed=RSS1.0";
        let url = Url::parse(url_text).unwrap();
        let feed_id = FeedID::new(url_text);
        let feed = feed_parser::download_and_parse_feed(&url, &feed_id, None, Arc::new(Semaphore::new(20)), &Client::new())
            .await
            .unwrap();

        let feed = match feed {
            ParsedUrl::SingleFeed(feed) => feed,
            ParsedUrl::MultipleFeeds(_) => panic!("Expected Single Feed"),
        };

        assert_eq!(feed.label, "Golem.de");
        assert_eq!(feed.website.unwrap().to_string(), "https://www.golem.de/");
    }

    #[tokio::test]
    pub async fn planet_gnome_rss() {
        let url_text = "http://planet.gnome.org/rss20.xml";
        let url = Url::parse(url_text).unwrap();
        let feed_id = FeedID::new(url_text);
        let feed = feed_parser::download_and_parse_feed(&url, &feed_id, None, Arc::new(Semaphore::new(20)), &Client::new())
            .await
            .unwrap();

        let feed = match feed {
            ParsedUrl::SingleFeed(feed) => feed,
            ParsedUrl::MultipleFeeds(_) => panic!("Expected Single Feed"),
        };

        assert_eq!(feed.label, "Planet GNOME");
        assert_eq!(feed.icon_url, None);
        assert_eq!(feed.website.unwrap().to_string(), "https://planet.gnome.org/");
    }

    #[tokio::test]
    pub async fn theverge_find_feeds() {
        let url_text = "https://www.theverge.com/";
        let url = Url::parse(url_text).unwrap();
        let feed_id = FeedID::new(url_text);
        let feed_vec = feed_parser::download_and_parse_feed(&url, &feed_id, None, Arc::new(Semaphore::new(20)), &Client::new())
            .await
            .unwrap();

        let feed = match feed_vec {
            ParsedUrl::MultipleFeeds(_) => panic!("Expected single Feed"),
            ParsedUrl::SingleFeed(feed) => feed,
        };

        assert_eq!(feed.label, "The Verge -  All Posts");
        assert_eq!(feed.feed_url.unwrap().as_str(), "https://www.theverge.com/rss/index.xml");
    }

    #[tokio::test]
    pub async fn paulstamatiou_find_feeds() {
        let url_text = "https://paulstamatiou.com";
        let url = Url::parse(url_text).unwrap();
        let feed_id = FeedID::new(url_text);
        let parsed_feed = feed_parser::download_and_parse_feed(&url, &feed_id, None, Arc::new(Semaphore::new(20)), &Client::new())
            .await
            .unwrap();

        let feed = match parsed_feed {
            ParsedUrl::MultipleFeeds(_) => panic!("Expected Single Feeds"),
            ParsedUrl::SingleFeed(feed) => feed,
        };

        assert_eq!(feed.label, "PaulStamatiou.com - Technology, Design and Photography");
        assert_eq!(feed.feed_url.unwrap().to_string(), "https://paulstamatiou.com/posts.xml");
    }
}
