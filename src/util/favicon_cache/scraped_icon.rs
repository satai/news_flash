use super::icon_info::IconInfo;

pub struct ScrapedIcon {
    pub info: IconInfo,
    pub mime: Option<String>,
    pub etag: Option<String>,
    pub data: Vec<u8>,
}
