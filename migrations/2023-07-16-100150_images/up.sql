DROP TRIGGER IF EXISTS on_delete_article_trigger;

CREATE TABLE images (
    image_url TEXT PRIMARY KEY NOT NULL,
    article_id TEXT NOT NULL,
    file_path TEXT NOT NULL
);

CREATE TRIGGER on_delete_article_trigger
    BEFORE DELETE ON articles
    BEGIN
        DELETE FROM taggings WHERE taggings.article_id=OLD.article_id;
        DELETE FROM enclosures WHERE enclosures.article_id=OLD.article_id;
        DELETE FROM thumbnails WHERE thumbnails.article_id=OLD.article_id;
        DELETE FROM images WHERE images.article_id=OLD.article_id;
    END;