pub mod metadata;

use self::metadata::LocalMetadata;
use crate::error::FeedParserError;
use crate::models::{
    self, ArticleID, Category, CategoryID, FatArticle, FavIcon, Feed, FeedID, FeedUpdateResult, LoginData, Marked, PluginCapabilities, Read,
    SyncResult, TagID, Url,
};
use crate::util;
use crate::util::favicon_cache::EXPIRES_AFTER_DAYS;
use crate::util::text2html::Text2Html;
use crate::{
    feed_api::{FeedApi, FeedApiError, FeedApiResult, Portal},
    models::Enclosure,
};
use async_trait::async_trait;
use chrono::{Duration, Utc};
use feed_rs::parser::ParseFeedError;
use feed_rs::{
    model::{Entry, Link, MediaContent, MediaObject},
    parser,
};
use futures::future;
use log::{error, warn};
use reqwest::Client;
use std::collections::HashMap;
use std::str;
use std::sync::Arc;
use tokio::sync::RwLock;

impl From<ParseFeedError> for FeedApiError {
    fn from(error: ParseFeedError) -> FeedApiError {
        match error {
            ParseFeedError::ParseError(e) => FeedApiError::Api {
                message: format!("Error parsing feed: {}", e),
            },
            ParseFeedError::IoError(e) => FeedApiError::IO(e),
            ParseFeedError::JsonSerde(e) => FeedApiError::Json {
                source: e,
                json: "Unavailable".into(),
            },
            ParseFeedError::JsonUnsupportedVersion(e) => FeedApiError::Api {
                message: format!("Unsupported Json feed: {}", e),
            },
            ParseFeedError::XmlReader(e) => FeedApiError::Api {
                message: format!("Error parsing xml: {}", e),
            },
        }
    }
}

pub struct LocalRSS {
    portal: Arc<Box<dyn Portal>>,
}

impl LocalRSS {
    fn select_article_url(links: &[Link]) -> Option<Url> {
        let mut url = links
            .iter()
            .find(|l| l.rel.as_deref() == Some("alternate"))
            .and_then(|l| Url::parse(&l.href).ok());

        if url.is_none() {
            for link in links {
                if let Ok(parsed_url) = Url::parse(&link.href) {
                    url = Some(parsed_url);
                    break;
                }
            }
        }

        url
    }

    async fn convert_entry(
        entry: Entry,
        feed_id: FeedID,
        website: Option<Url>,
        enclosures: Arc<RwLock<Vec<Enclosure>>>,
        portal: Arc<Box<dyn Portal>>,
    ) -> Option<FatArticle> {
        let Entry {
            id,
            updated,
            title,
            authors,
            content,
            links,
            summary: entry_summary,
            categories: _,
            contributors: _,
            published,
            source: _,
            rights: _,
            media,
            language: _,
            base: _,
        } = entry;

        let article_id = ArticleID::new(&id);
        let article_url = Self::select_article_url(&links);

        let local_article = portal.get_articles(&[article_id.clone()]).ok().and_then(|v| v.first().cloned());

        let mut marked = Marked::Unmarked;
        let mut unread = Read::Unread;

        // if article exists in db and already has same timestamp as 'updated' then skip it
        if let Some(local_article) = local_article {
            if let Some(updated) = entry.updated {
                if local_article.date >= updated.naive_utc() {
                    return None;
                }
            } else {
                // we have no updated timestamp for this article and it already exists in the db
                // so we skip this one
                return None;
            }

            marked = local_article.marked;
            unread = local_article.unread;
        }

        // FIXME: handle content-type
        let xml_base = content
            .as_ref()
            .and_then(|c| c.src.as_ref().map(|l| l.href.clone()))
            .and_then(|xb| Url::parse(&xb).ok());
        let mut scraped_thumbnail = None;
        let html = match content.and_then(|c| c.body) {
            Some(html) => {
                let xml_base = xml_base.map(Some).unwrap_or(website);
                let cleaned_html = xml_base.and_then(|xml_base| article_scraper::clean::clean_html_fragment(&html, &xml_base).ok());

                if let Some(clean_result) = cleaned_html {
                    scraped_thumbnail = clean_result.thumbnail;
                    Some(clean_result.html)
                } else {
                    Some(html)
                }
            }
            None => media.first().and_then(|m| m.description.as_ref()).map(|t| t.content.clone()),
        };
        let plain_text = match &entry_summary {
            Some(summary) => Some(util::html2text::html2text(&summary.content)),
            None => html.as_deref().map(util::html2text::html2text),
        };
        let summary = plain_text.as_deref().map(util::html2text::text2summary);
        let html = match html {
            Some(html) => Some(html),
            None => match &entry_summary {
                Some(original_summary) => Some(original_summary.content.clone()),
                None => plain_text.clone(),
            },
        };

        let html = html.map(|s| if !Text2Html::is_html(&s) { Text2Html::process(&s) } else { s });

        let mut thumbnail_url = media
            .iter()
            .filter_map(|media| {
                let MediaObject { content, thumbnails, .. } = media;
                let attached_images: Vec<Url> = content
                    .iter()
                    .filter_map(|content| {
                        let MediaContent { url, content_type, .. } = content;
                        content_type.as_ref().and_then(|mime| {
                            if mime.ty() == "image" {
                                url.as_ref().map(|url| Url::new(url.clone()))
                            } else {
                                None
                            }
                        })
                    })
                    .collect();
                let thumbnails = thumbnails.iter().map(|t| t.image.uri.as_str()).collect::<Vec<&str>>();
                if let Some(&thumbnail) = thumbnails.first() {
                    Some(thumbnail.to_owned())
                } else {
                    attached_images.first().map(|first_image| first_image.to_string())
                }
            })
            .collect::<Vec<String>>()
            .first()
            .cloned();
        if thumbnail_url.is_none() {
            thumbnail_url = scraped_thumbnail;
        }

        let mut entry_enclosures = media
            .iter()
            .flat_map(|media| {
                let MediaObject { content, .. } = media;
                content
                    .iter()
                    .filter_map(|content| {
                        let MediaContent { url, content_type, .. } = content;
                        url.as_ref().map(|url| {
                            let url = Url::new(url.clone());

                            Enclosure {
                                article_id: article_id.clone(),
                                url,
                                mime_type: content_type.as_ref().map(|mime| mime.to_string()),
                                title: None,
                                position: None,
                            }
                        })
                    })
                    .collect::<Vec<Enclosure>>()
            })
            .collect::<Vec<Enclosure>>();
        enclosures.write().await.append(&mut entry_enclosures);

        let article = FatArticle {
            article_id,
            feed_id,
            title: title.map(|t| match escaper::decode_html(&t.content) {
                Ok(title) => title,
                Err(_error) => {
                    // This warning freaks users out for some reason
                    // warn!("Error {:?} at character {}", error.kind, error.position);
                    t.content
                }
            }),
            url: article_url,
            author: authors
                .iter()
                .filter_map(|person| if person.name.is_empty() { None } else { Some(person.name.clone()) })
                .next(),
            date: match published {
                Some(published) => published,
                None => match updated {
                    Some(updated) => updated,
                    None => Utc::now(),
                },
            }
            .naive_utc(),
            synced: Utc::now().naive_utc(),
            direction: None,
            marked,
            unread,
            html,
            scraped_content: None,
            summary,
            plain_text,
            thumbnail_url,
        };

        Some(article)
    }

    async fn download_feed(url: &Url, client: &Client) -> Result<feed_rs::model::Feed, (String, FeedApiError)> {
        let feed_request = match client.get(url.as_str()).send().await {
            Ok(response) => response,
            Err(error) => {
                let msg = format!("Downloading feed failed: {} - {}", url, error);
                error!("{msg}");
                return Err((msg, FeedApiError::Network(error)));
            }
        };

        if !feed_request.status().is_success() {
            let msg = format!("Downloading feed failed: {} -  {}", feed_request.status(), url);
            error!("{msg}");

            if let Err(error) = feed_request.error_for_status() {
                return Err((msg, FeedApiError::Network(error)));
            } else {
                return Err((msg, FeedApiError::Unknown));
            }
        }

        let content_location = feed_request.headers().get(reqwest::header::CONTENT_LOCATION).cloned();

        let result_bytes = match feed_request.bytes().await {
            Ok(result_bytes) => result_bytes,
            Err(error) => {
                let msg = format!("Reading response as string failed: {url} - {error}");
                error!("{msg}");
                return Err((msg, FeedApiError::Network(error)));
            }
        };

        let feed_base_url = if let Some(content_location) = &content_location {
            let content_location = content_location.to_str().ok();
            content_location.and_then(|cl| Url::parse(cl).ok())
        } else {
            url.base().ok()
        };
        let feed_base_url = feed_base_url.map(|uri| uri.to_string());

        let parser = parser::Builder::new().base_uri(feed_base_url).build();
        let parse_result = parser.parse(result_bytes.as_ref());

        let parsed_feed = if let Ok(parsed_feed) = parse_result {
            parsed_feed
        } else {
            let msg = "Couldn't parse feed content";
            error!("{msg}");
            return Err((msg.into(), FeedApiError::ParseFeed(FeedParserError::Feed)));
        };

        Ok(parsed_feed)
    }
}

#[async_trait]
impl FeedApi for LocalRSS {
    fn features(&self) -> FeedApiResult<PluginCapabilities> {
        Ok(PluginCapabilities::ADD_REMOVE_FEEDS
            | PluginCapabilities::SUPPORT_CATEGORIES
            | PluginCapabilities::MODIFY_CATEGORIES
            | PluginCapabilities::SUPPORT_TAGS
            | PluginCapabilities::EDIT_FEED_URLS)
    }

    fn has_user_configured(&self) -> FeedApiResult<bool> {
        Ok(true)
    }

    async fn is_logged_in(&self, _client: &Client) -> FeedApiResult<bool> {
        Ok(true)
    }

    async fn user_name(&self) -> Option<String> {
        None
    }

    async fn get_login_data(&self) -> Option<LoginData> {
        Some(LoginData::None(LocalMetadata::get_id()))
    }

    async fn login(&mut self, _data: LoginData, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn logout(&mut self, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn initial_sync(&self, client: &Client) -> FeedApiResult<SyncResult> {
        let feeds = self.portal.get_feeds()?;
        let semaphore = self.portal.get_download_semaphore();

        let feed_hash_map = Arc::new(RwLock::new(
            feeds
                .clone()
                .into_iter()
                .map(|feed| (feed.feed_id.clone(), feed))
                .collect::<HashMap<FeedID, Feed>>(),
        ));

        let mut task_handles = Vec::new();

        for feed in feeds.into_iter() {
            let client = client.clone();
            let portal = self.portal.clone();
            let hash_map = feed_hash_map.clone();
            let semaphore = Arc::clone(&semaphore); //.acquire_owned().await;

            task_handles.push(tokio::spawn(async move {
                let mut articles: Vec<FatArticle> = Vec::new();
                let enclosures: Arc<RwLock<Vec<Enclosure>>> = Arc::new(RwLock::new(Vec::new()));

                let url = if let Some(url) = feed.feed_url.clone() {
                    url
                } else {
                    let msg = format!("No feed url for feed: '{}'", feed.feed_id);
                    warn!("{msg}");
                    if let Some(old_feed) = hash_map.write().await.get_mut(&feed.feed_id) {
                        old_feed.error_count += 1;
                        old_feed.error_message = Some(msg);
                    }
                    return Err(FeedApiError::ParseFeed(FeedParserError::NoUrl));
                };

                // acquire permit first to make sure only a set number of feeds can download concurrently
                let permit = match semaphore.acquire().await {
                    Ok(permit) => permit,
                    Err(error) => {
                        log::error!("couldn't acquire download permit for feed {url}: {error}");
                        return Err(error.into());
                    }
                };

                let parsed_feed = match Self::download_feed(&url, &client).await {
                    Ok(parsed_feed) => parsed_feed,
                    Err((msg, error)) => {
                        if let Some(old_feed) = hash_map.write().await.get_mut(&feed.feed_id) {
                            old_feed.error_count += 1;
                            old_feed.error_message = Some(msg);
                        }

                        return Err(error);
                    }
                };

                // deliberatly drop permit here to allow other feeds to download now
                drop(permit);

                // update icon url & website
                let updated_feed = Feed::from_feed_rs(parsed_feed.clone(), None, &url);
                if let Some(old_feed) = hash_map.write().await.get_mut(&feed.feed_id) {
                    old_feed.icon_url = updated_feed.icon_url;
                    old_feed.website = updated_feed.website;
                    old_feed.error_count = 0;
                    old_feed.error_message = None;
                }

                let tasks = parsed_feed
                    .entries
                    .into_iter()
                    .map(|e| {
                        let feed_id = feed.feed_id.clone();
                        let website = feed.website.clone();
                        let portal = portal.clone();
                        let enclosures = enclosures.clone();
                        tokio::spawn(async move { Self::convert_entry(e, feed_id, website, enclosures, portal).await })
                    })
                    .collect::<Vec<_>>();

                let task_results = future::join_all(tasks).await;
                let mut feed_articles = task_results.into_iter().filter_map(|r| r.ok()).flatten().collect();

                articles.append(&mut feed_articles);

                Ok((articles, Arc::into_inner(enclosures).map(|e| e.into_inner()).unwrap_or_default()))
            }));
        }

        let mut articles: Vec<FatArticle> = Vec::new();
        let mut enclosures: Vec<Enclosure> = Vec::new();

        let result_vec = futures::future::join_all(task_handles).await;

        for (mut feed_articles, mut feed_enclosures) in result_vec.into_iter().flatten().flatten() {
            articles.append(&mut feed_articles);
            enclosures.append(&mut feed_enclosures);
        }

        let feeds = Arc::into_inner(feed_hash_map)
            .map(|lock| lock.into_inner())
            .map(|map| map.into_values().collect())
            .and_then(util::vec_to_option);

        Ok(SyncResult {
            feeds,
            categories: None,
            feed_mappings: None,
            category_mappings: None,
            tags: None,
            headlines: None,
            articles: util::vec_to_option(articles),
            enclosures: util::vec_to_option(enclosures),
            taggings: None,
        })
    }

    async fn sync(&self, client: &Client) -> FeedApiResult<SyncResult> {
        self.initial_sync(client).await
    }

    async fn fetch_feed(&self, feed_id: &FeedID, client: &Client) -> FeedApiResult<FeedUpdateResult> {
        let feeds = self.portal.get_feeds()?;
        let mut feed = feeds.into_iter().find(|f| &f.feed_id == feed_id).ok_or(FeedApiError::Unknown)?;

        let enclosures: Arc<RwLock<Vec<Enclosure>>> = Arc::new(RwLock::new(Vec::new()));

        let url = feed.feed_url.clone().ok_or(FeedApiError::Unknown)?;

        let parsed_feed = match Self::download_feed(&url, client).await {
            Ok(parsed_feed) => parsed_feed,
            Err((_msg, error)) => {
                return Err(error);
            }
        };

        // update icon url & website
        let updated_feed = Feed::from_feed_rs(parsed_feed.clone(), None, &url);
        feed.icon_url = updated_feed.icon_url;
        feed.website = updated_feed.website;
        feed.error_count = 0;
        feed.error_message = None;

        let tasks = parsed_feed
            .entries
            .into_iter()
            .map(|e| {
                let feed_id = feed.feed_id.clone();
                let website = feed.website.clone();
                let portal = self.portal.clone();
                let enclosures = enclosures.clone();
                tokio::spawn(async move { Self::convert_entry(e, feed_id, website, enclosures, portal).await })
            })
            .collect::<Vec<_>>();

        let task_results = future::join_all(tasks).await;
        let articles: Vec<FatArticle> = task_results.into_iter().filter_map(|r| r.ok()).flatten().collect();
        let enclosures = Arc::into_inner(enclosures).map(|e| e.into_inner()).unwrap_or_default();

        Ok(FeedUpdateResult {
            feed: Some(feed),
            articles: util::vec_to_option(articles),
            enclosures: util::vec_to_option(enclosures),
            taggings: None,
        })
    }

    async fn set_article_read(&self, _articles: &[ArticleID], _read: models::Read, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn set_article_marked(&self, _articles: &[ArticleID], _marked: models::Marked, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn set_feed_read(&self, _feeds: &[FeedID], _articles: &[ArticleID], _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn set_category_read(&self, _categories: &[CategoryID], _articles: &[ArticleID], _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn set_tag_read(&self, _tags: &[TagID], _articles: &[ArticleID], _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn set_all_read(&self, _articles: &[ArticleID], _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn add_feed(
        &self,
        url: &Url,
        title: Option<String>,
        category_id: Option<CategoryID>,
        client: &Client,
    ) -> FeedApiResult<(Feed, Option<Category>)> {
        let feed_response = client.get(url.as_str()).send().await?.error_for_status()?;
        let result_bytes = feed_response.bytes().await.map_err(|err| {
            error!("Reading response as bytes failed: {}", url);
            err
        })?;

        let parser = parser::Builder::new().base_uri(url.base().ok().as_deref()).build();
        let feed = parser.parse(result_bytes.as_ref())?;
        let feed = Feed::from_feed_rs(feed, title, url);

        let categories = self.portal.get_categories()?;
        let category = categories.iter().find(|c| Some(&c.category_id) == category_id.as_ref()).cloned();

        Ok((feed, category))
    }

    async fn remove_feed(&self, _id: &FeedID, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn move_feed(&self, _feed_id: &FeedID, _from: &CategoryID, _to: &CategoryID, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn rename_feed(&self, feed_id: &FeedID, _new_title: &str, _client: &Client) -> FeedApiResult<FeedID> {
        Ok(feed_id.clone())
    }

    async fn edit_feed_url(&self, _feed_id: &FeedID, _new_url: &str, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn add_category(&self, title: &str, _parent: Option<&CategoryID>, _client: &Client) -> FeedApiResult<CategoryID> {
        Ok(CategoryID::new(title))
    }

    async fn remove_category(&self, _id: &CategoryID, _remove_children: bool, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn rename_category(&self, id: &CategoryID, _new_title: &str, _client: &Client) -> FeedApiResult<CategoryID> {
        Ok(id.clone())
    }

    async fn move_category(&self, _id: &CategoryID, _parent: &CategoryID, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn import_opml(&self, _opml: &str, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn add_tag(&self, title: &str, _client: &Client) -> FeedApiResult<TagID> {
        Ok(TagID::new(title))
    }

    async fn remove_tag(&self, _id: &TagID, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn rename_tag(&self, id: &TagID, _new_title: &str, _client: &Client) -> FeedApiResult<TagID> {
        Ok(id.clone())
    }

    async fn tag_article(&self, _article_id: &ArticleID, _tag_id: &TagID, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn untag_article(&self, _article_id: &ArticleID, _tag_id: &TagID, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn get_favicon(&self, feed_id: &FeedID, client: &Client) -> FeedApiResult<FavIcon> {
        if let Some(feed) = self.portal.get_feeds()?.into_iter().find(|f| &f.feed_id == feed_id) {
            if let Some(feed_url) = feed.feed_url.as_ref() {
                let downloaded_feed = Self::download_feed(feed_url, client).await.map_err(|(_msg, err)| err)?;
                let updated_feed = Feed::from_feed_rs(downloaded_feed.clone(), None, feed_url);

                Ok(FavIcon {
                    feed_id: feed_id.clone(),
                    expires: Utc::now().naive_utc() + Duration::try_days(EXPIRES_AFTER_DAYS).unwrap(),
                    format: None,
                    etag: None,
                    source_url: updated_feed.icon_url,
                    data: None,
                })
            } else if let Some(icon_url) = feed.icon_url {
                Ok(FavIcon {
                    feed_id: feed_id.clone(),
                    expires: Utc::now().naive_utc() + Duration::try_days(EXPIRES_AFTER_DAYS).unwrap(),
                    format: None,
                    etag: None,
                    source_url: Some(icon_url),
                    data: None,
                })
            } else {
                Err(FeedApiError::Unknown)
            }
        } else {
            Err(FeedApiError::Unknown)
        }
    }
}
