pub const SUMMARY_LEN: usize = 300;

pub fn html2text(html: &str) -> String {
    nanohtml2text::html2text(html).trim().into()
}

pub fn text2summary(plain_text: &str) -> String {
    let text: String = plain_text.chars().take(SUMMARY_LEN).collect();
    text.replace('\n', " ")
}

#[cfg(test)]
mod tests {
    #[test]
    pub fn hardwareluxx() {
        let article = "<p><img src=\"https://www.hardwareluxx.de/images/stories/2017/stadia.jpg\" alt=\"stadia\">Am vergangenen Dienstag präsentierte Google im Rahmen der Game Developers Conference in San Francisco seinen neuen  <a href=\"https://www.hardwareluxx.de/index.php/news/software/spiele/48994-googles-cloud-gaming-plattform-stadia-geht-an-den-start.html\" rel=\"noopener noreferrer\" target=\"_blank\" referrerpolicy=\"no-referrer\">Spiele-Streaming-Dienst Stadia</a> , der noch im Sommer dieses Jahres an den Start gehen soll. Auch einen eigenen  <a href=\"https://www.hardwareluxx.de/index.php/news/hardware/eingabegeraete/49001-googles-stadia-controller-mit-zwei-sondertasten-zum-heimlichen-star.html\" rel=\"noopener noreferrer\" target=\"_blank\" referrerpolicy=\"no-referrer\">Controller mit vielen interessanten Features</a>  hatte der Konzern den anwesenden Journalisten gezeigt.</p><p>Die Vorteile von Stadia liegen klar auf der Hand: Die Hardware im Rechenzentrum ist dank skalierbarer Infrastruktur schneller als jede Heimkonsole und jeder Spiele-PC zu Hause und erlaubt damit theoretisch die höchste Bildqualität. Hinzu kommt, dass langwierige Downloads und Installations-Prozesse entfallen und teure Hardware für die Nutzung des Dienstes nicht benötigt wird. Ein leistungsschwaches Notebook oder gar ein herkömmliches Smartphone sollen laut Google genügen.</p>";
        let needle = "Am vergangenen Dienstag";

        let summary = super::html2text(article);

        assert_eq!(needle, &summary[..needle.len()]);
    }

    #[test]
    pub fn golem() {
        let article = "Was früher eine Auszeichnung war, ist vielen Twitter-Nutzern nun eher peinlich. (<a href=\"https://www.golem.de/specials/twitter/\">Twitter</a>, <a href=\"https://www.golem.de/specials/api/\">API</a>) <img src=\"https://cpx.golem.de/cpx.php?class=17&amp;aid=176395&amp;page=1&amp;ts=1690993502\" alt=\"\" width=\"1\" height=\"1\" />";
        let needle = "Was früher eine Auszeichnung war, ist vielen Twitter-Nutzern nun eher peinlich.";

        let summary = super::html2text(article);

        assert_eq!(needle, &summary[..needle.len()]);
    }
}
