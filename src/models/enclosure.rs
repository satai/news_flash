use crate::models::ArticleID;
use crate::models::Url;
use crate::schema::enclosures;

#[derive(Identifiable, Queryable, Debug, Insertable, Clone)]
#[diesel(primary_key(article_id))]
#[diesel(table_name = enclosures)]
pub struct Enclosure {
    pub article_id: ArticleID,
    pub url: Url,
    pub mime_type: Option<String>,
    pub title: Option<String>,
    pub position: Option<f64>,
}

impl PartialEq for Enclosure {
    fn eq(&self, other: &Self) -> bool {
        self.article_id == other.article_id && self.url == other.url
    }
}

impl Eq for Enclosure {}
